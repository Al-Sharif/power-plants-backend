import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RouterModule } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import { getEnvPath } from './assets/common/helpers/env.helper';
import { ChartsModule } from './modules/charts/charts.module';
const envFilePath: string = getEnvPath(`${__dirname}/assets/common/envs`);
@Module({
  imports: [
    RouterModule.register([
      {
        path: 'charts',
        module: ChartsModule
      }
    ]),
    ConfigModule.forRoot({ envFilePath, isGlobal: true }),
    MongooseModule.forRoot(process.env.DATA_STORE),
    ChartsModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
