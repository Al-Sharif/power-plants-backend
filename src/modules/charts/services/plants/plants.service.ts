import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, PipelineStage } from 'mongoose';
import { PlantsDetailsResultDto } from '../../dto/plants/plants-details-result.dto';
import { StatesPlantsResultDto } from '../../dto/plants/states-plants-result.dto';
import { Plant, PlantDocument } from '../../schemas/plant.schema';

@Injectable()
export class PlantsService {
    constructor(@InjectModel(Plant.name) private plantModel: Model<PlantDocument>) { }

    async getStates(): Promise<StatesPlantsResultDto[]> {
        const totalAnnualNetGeneration = await this.getAnnualNetGenerationOverall();
        const statesStats = await this.plantModel.aggregate<StatesPlantsResultDto>([
            {
                $group: {
                    _id: "$state",
                    count: { $sum: 1 },
                    annualNetGeneration: {
                        $sum: "$annualNetGeneration"
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    state: "$_id",
                    totalPlants: "$count",
                    annualNetGeneration: "$annualNetGeneration",
                    percentage: {
                        $multiply: [{ $divide: ["$annualNetGeneration", totalAnnualNetGeneration] }, 100]
                    },
                }
            },
            { $sort: { annualNetGeneration: -1 } }

        ])
        return statesStats;
    }

    async getStatePlants(state: string): Promise<PlantsDetailsResultDto[]> {
        return this.plantModel.find({ state: state }).select({ _id: 0 });
    }

    async getTopPlants(numberOfPlants: number, state: string) {
        const pipeline: PipelineStage[] = [];
        if (state && state != '') {
            pipeline.push({
                $match: { state: state }
            })
        }
        const initialPipelineStages: PipelineStage[] = this.getInitialTopPipelineStages(numberOfPlants)
        pipeline.push(...initialPipelineStages);
        return await this.plantModel.aggregate(pipeline)
    }

    private getInitialTopPipelineStages(numberOfPlants: number): PipelineStage[] {
        return [
            {
                $sort: { annualNetGeneration: -1 }
            },
            { $limit: numberOfPlants },
            {
                $project: {
                    _id: 0
                }
            }
        ];
    }

    private async getAnnualNetGenerationOverall() {
        const result = await this.plantModel.aggregate([
            {
                $group: {
                    _id: null,
                    total: {
                        $sum: "$annualNetGeneration"
                    }
                }
            }
        ])

        const totaltotal = result && result.length > 0 ? result[0].total : 0;

        return totaltotal;
    }
}
