import { Controller, Get, Param, Query } from '@nestjs/common';
import { PlantsDetailsResultDto } from '../../dto/plants/plants-details-result.dto';
import { StatesPlantsResultDto } from '../../dto/plants/states-plants-result.dto';
import { PlantsService } from '../../services/plants/plants.service';

@Controller('plants')
export class PlantsController {
    constructor(private readonly plantsService: PlantsService) { }

    @Get()
    getStates(): Promise<StatesPlantsResultDto[]> {
        return this.plantsService.getStates();
    }

    @Get('state-plants/:state') 
    getStatePlants(@Param() params): Promise<PlantsDetailsResultDto[]> {
        return this.plantsService.getStatePlants(params.state);
    }

    @Get('top')
    getTopPlants(@Query() query): Promise<PlantsDetailsResultDto[]> {
        return this.plantsService.getTopPlants(parseInt(query.numberOfPlants), query.state);
    }
}
