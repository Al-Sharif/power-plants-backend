import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PlantDocument = Plant & Document;

@Schema({collection: 'plants'})
export class Plant {
  @Prop({required: true})
  year: number;

  @Prop({required: true})
  state: string;

  @Prop()
  name: string;

  @Prop()
  owner: string;

  @Prop()
  balancingAuthorityName: string;

  @Prop()
  countyName: string;

  @Prop()
  latitude: string;

  @Prop()
  longitude: string;

  @Prop()
  unitsCount: number;

  @Prop()
  generatorsCount: number;

  @Prop()
  annualNetGeneration: number;
}

export const PlantSchema = SchemaFactory.createForClass(Plant);