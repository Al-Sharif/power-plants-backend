export class StatesPlantsResultDto {
    state: string;
    totalPlants: number;
    annualNetGeneration: number;
    percentage: number;
}