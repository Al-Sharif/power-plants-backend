import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Plant, PlantSchema } from './schemas/plant.schema';
import { PlantsController } from './controllers/plants/plants.controller';
import { PlantsService } from './services/plants/plants.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Plant.name, schema: PlantSchema }])
  ],
  controllers: [PlantsController],
  providers: [PlantsService]
})
export class ChartsModule { }
